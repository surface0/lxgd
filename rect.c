/* rect.c */

/**
 * HP200LX Graphics Library
 * <pset>
 *
 * @author surface0
 */

#include "lxgd.h"
#include "common.h"

#define max(a, b) ((a) > (b)) ? (a) : (b)
#define min(a, b) ((a) < (b)) ? (a) : (b)

/* 短形 */
void DrawRectAngle (const RECTANGLE *rectAngle)
{   
    DrawLine (rectAngle->Points[0].X,
              rectAngle->Points[0].Y,
              rectAngle->Points[1].X,
              rectAngle->Points[0].Y);
       
    DrawLine (rectAngle->Points[1].X,
              rectAngle->Points[0].Y,
              rectAngle->Points[1].X,
              rectAngle->Points[1].Y);
              
    DrawLine (rectAngle->Points[1].X,
              rectAngle->Points[1].Y,
              rectAngle->Points[0].X,
              rectAngle->Points[1].Y);
    
    DrawLine (rectAngle->Points[0].X,
              rectAngle->Points[0].Y,
              rectAngle->Points[0].X,
              rectAngle->Points[1].Y);
}

void DrawRectAngle2 (const RECTANGLE *ra)
{
    int sp, ep;
    
    /* 上辺下辺描画 */
    sp = min(ra->Rect.Left, ra->Rect.Right);
    ep = max(ra->Rect.Left, ra->Rect.Right);
    do
    {
      PSet(sp, ra->Rect.Top);
      PSet(ep, ra->Rect.Top);
      PSet(sp, ra->Rect.Bottom);
      PSet(ep, ra->Rect.Bottom);
    }
    while(sp++ <= ep--);
    
    /* 左辺右辺描画 */
    sp = min(ra->Rect.Top, ra->Rect.Bottom);
    ep = max(ra->Rect.Top, ra->Rect.Bottom);
    do
    {
      PSet(sp, ra->Rect.Left);
      PSet(ep, ra->Rect.Left);
      PSet(sp, ra->Rect.Right);
      PSet(ep, ra->Rect.Right);
    }
    while(sp++ <= ep--);
    
    return;
}

/* 塗りつぶし短形 */
void FillRectAngle (const RECTANGLE *rectAngle)
{
    int s = min(rectAngle->Rect.Top, rectAngle->Rect.Bottom);
    int e = max(rectAngle->Rect.Top, rectAngle->Rect.Bottom);
    
    while (s++ <= e)
    {
        DrawLine (rectAngle->Rect.Left,
                  s,
                  rectAngle->Rect.Right,
                  s);
    }
}