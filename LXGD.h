/* lxgd.h */

/**
 * HP200LX Graphics Library
 * <lxgd>
 *
 * @author surface0
 */

#ifndef DEFINE_LXGD
#define DEFINE_LXGD

#include "common.h"

/* POINT struct */
typedef struct {
    UINT16 X;
    UINT16 Y;
} POINT;

/* RECT struct */
typedef struct {
    UINT16 Left;
    UINT16 Top;
    UINT16 Right;
    UINT16 Bottom;
} RECT;

/* RECTANGLE union */
typedef union {
    POINT   Points[2];
    RECT    Rect;
} RECTANGLE;

/* dot prot */
#ifdef DEBUG_PSET
void PSet (const int x, const int y);
#else
#define PSet(x, y)                                          \
    if (x < 640 && y < 200 && x >= 0 && y >= 0)             \
    {                                                       \
        *((UCHAR far*)(0xb8000000+0x50*(y/2) +              \
		((y)&0x01) * 0x2000 + (x) / 8)) |=                      \
		(UCHAR)(0x80 >> (x&0x07));                          \
    }
#endif

/* line */
void DrawLine(int x0, int y0, int x1, int y1);
void DrawLineP(const POINT *start, const POINT *end);
void DrawLineR(const RECT *rect);

/* rect */
void DrawRectAngle(const RECTANGLE *rectAngle);
void DrawRectAngle2(const RECTANGLE *ra);

/* fill rect */
void FillRectAngle(const RECTANGLE *rectAngle);

/* circle */
void DrawCircle(const int x0, const int y0, int r);
/* ellipse */
void DrawEllipse(const int x0, const int y0, int r, const int a, const int b);
#endif
