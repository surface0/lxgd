/* circle.c */

/**
 * HP200LX Graphics Library
 * <clrcle>
 *
 * @authro surface0
 */

#include "lxgd.h"
#include "common.h"
#include <math.h>

/* DrawCircle */
void DrawCircle(const int x0, const int y0, int r)
{
  int y,F;

  y = 0;
  F = -2*r + 3;

  while (r >= y)
  {
    PSet(x0+r, y0+y);
    PSet(x0-r, y0+y);
    PSet(x0+r, y0-y);
    PSet(x0-r, y0-y);
    PSet(x0+y, y0+r);
    PSet(x0-y, y0+r);
    PSet(x0+y, y0-r);
    PSet(x0-y, y0-r);
    
    if (F >= 0)
    {
        r--;
        F -= 4*r;
    }
    y++;
    F += 4*y + 2;
  }
    return;
}

/* DrawEllipse */
void DrawEllipse(const int x0, const int y0, int r, const int a, const int b)
{
  int y, F, H;

  y = 0;
  F = -2 * sqrt((double)a) * r + a + 2*b;
  H = -4 * sqrt((double)a) * r + 2*a + b;
  r /= sqrt((double)a);
  
  while (r > 0) {
    PSet(x0+r, y0+y);
    PSet(x0-r, y0+y);
    PSet(x0+r, y0-y);
    PSet(x0-r, y0-y);
    if (F < 0) {
      y++;
      F += 4 * b * y + 2 * b;
      H += 4 * b * y;
    } else if (H >= 0) {
      r--;
      F -= 4*a*r;
      H -= 4*a*r - 2*a;
    } else {
      r--;
      y++;
      F += 4*b*y - 4*a*r + 2*b;
      H += 4*b*y - 4*a*r + 2*a;
    }
  }
}

