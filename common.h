/* coomon.h */

/**
 * HP200LX Graphics Library
 * <common>
 *
 * @author surface0
 */

#ifndef DEFINE_LXGD_COMMON
#define DEFINE_LXGD_COMMON

#define UINT32  unsigned long
#define UINT16  unsigned short
#define UCHAR   unsigned char

#endif