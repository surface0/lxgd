/* pset.c */

/**
 * HP200LX Graphics Library
 * <pset>
 *
 * @author surface0
 */

/* #define DEBUG_PSET */

#include "lxgd.h"
#include "common.h"
#include <stdio.h>

#ifdef DEBUG_PSET
FILE *fp_pset;

void PSet(const int x, const int y)
{
    if (x > 639 || y > 199 || x < 0 || y < 0) return;
    
    fprintf(fp_pset, "%d,%d,", x, y);
    
    return;
}
#endif

