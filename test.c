/* test_main.c */

#include <stdio.h>
#include <stdlib.h>
#include "lxgd.h"

#define OUTPUT_FILE "points.txt"

#ifdef DEBUG_PSET
extern FILE *fp_pset;
#endif

int main(void)
{
    RECTANGLE rect;
    int       i;
    
#ifdef DEBUG_PSET
    fp_pset = fopen(OUTPUT_FILE, "w");
#endif

	rect.Rect.Top       = 0;
	rect.Rect.Left      = 0;
	rect.Rect.Bottom    = 199;
	rect.Rect.Right     = 639;

	for (i = 0; i < 5; i++)
	{
		//DrawLineR (&rect.Rect);
		DrawCircle(319, 99, (i+1)*20);
    DrawEllipse(319,99, (i+1)*200, 5, 10);
		DrawRectAngle(&rect);
		rect.Rect.Top      += 10;
		rect.Rect.Left     += 10;
		rect.Rect.Bottom   -= 10;
		rect.Rect.Right    -= 10;

		//FillRectAngle (&rect);
	}

#ifdef DEBUG_PSET
	fclose(fp_pset);
	system("proter " OUTPUT_FILE);
#endif

	return 0;
}