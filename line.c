/* line.c */

/**
 * HP200LX Graphics Library
 * <line>
 *
 * @author surface0
 */

#include "lxgd.h"
#include "common.h"

/* �����`�� ��{�` */
void DrawLine (int x0, int y0, int x1, int y1)
{

  int E, dx, dy, sx, sy, i;
  
  sx = (x1 > x0) ? 1      : -1;
  dx = (x1 > x0) ? x1-x0  : x0-x1;
  sy = (y1 > y0) ? 1      : -1;
  dy = (y1 > y0) ? y1-y0  : y0-y1;

  if (dx >= dy)
  {
    E = -dx;
    for (i = 0; i < ((dx + 1)>>1); i++)
    {
      PSet(x0, y0);
      PSet(x1, y1);
      x0 += sx;
      x1 -= sx;
      E += 2 * dy;
      
      if (E >= 0)
      {
        y0 += sy;
        y1 -= sy;
        E -= 2 * dx;
      } /* end of if */
    }/* end of for */
      if (((dx + 1)%2) != 0)
         PSet(x0, y0);
  }/* end of if */
  else
  {
    E = -dy;
    for (i = 0; i < ((dy + 1) >> 1); i++)
    {
      PSet(x0, y0);
      PSet(x1, y1);
      y0 += sy;
      y1 -= sy;
      E += 2 * dx;
      if (E >= 0)
      {
        x0 += sx;
        x1 -= sx;
        E -= 2 * dy;
      }/* end of if */
    }/* end of for */
    if (((dy + 1)%2) != 0)
      PSet(x0, y0);
  }
}

/* �����`�� RECT�w�� */
void DrawLineR (const RECT *rect)
{
    DrawLine ((int)rect->Left,
              (int)rect->Top,
              (int)rect->Right,
              (int)rect->Bottom);
}

/* �����`�� POINT�w�� */
void DrawLineP (const POINT *start, const POINT *end)
{
    DrawLine ((int)start->X, (int)start->Y, (int)end->X, (int)end->Y);
}
